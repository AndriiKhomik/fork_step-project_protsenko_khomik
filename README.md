# Fork_step-project_Protsenko_Khomik

Проект ForkIO реализован в команде из 2-х человек: Хомик Андрей и Проценко Андрей. Проект собран с помощью таск-менеджера Gulp 4, в котором подключены следующие пакеты:
-  шаблонизатор PUG;
- препроцессор SCSS;
- browser-sync ;
- autoprefixer;
- imagemin;
Также проект реализован с помощью GRID CSS и Flexbox для адаптивного дизайна и с использованием методологии БЭМ.

Andrii Khomik верстал задание для студента №1
Шапку сайта с верхним меню
People Are Talking About Fork
Настроил сборку проекта Gulp
Создал Git репозиторий


Andrey Protsenko верстал задание для студента №2
Revolutionary Editor
Here is what you get
Fork Subscription Pricing
Внес поправки в сборку проекта Gulp
Разместил на GitLab Pages


Сылка на GitLab Pages https://andriikhomik.gitlab.io/fork_step-project_protsenko_khomik