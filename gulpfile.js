const gulp = require('gulp');
const concat = require('gulp-concat');
const del = require('delete');
let uglify = require('gulp-uglify-es').default;
const htmlmin = require('gulp-htmlmin');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass')(require('sass'));
const rename = require('gulp-rename');
const cssmin = require('gulp-cssmin');
const purgecss = require('gulp-purgecss');
const pug = require('gulp-pug');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

gulp.task('buildCss', function() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            cascade: true
        }))
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min'}))
        .pipe(purgecss({
            content: ['src/**/*.pug']
        }))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream())
});


gulp.task('buildHTML', function() {
    return gulp.src('src/*.pug')
        .pipe(htmlmin({ collapseWhitespace: false }))
        .pipe(
            pug({
                pretty: true
            }))
        .pipe(gulp.dest('dist'))
});

gulp.task('buildJs', function() {
    return gulp.src('src/js/*.js')
        .pipe(uglify())
        .pipe(rename({ suffix: '.min'}))
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream())
});

gulp.task('buildImages', function(){
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
});

gulp.task('clean', function(){
    return del('dist/**', { force: true });
});

gulp.task('dev', function() {
    browserSync.init({
        server: { baseDir: 'dist/' }, 
        notify: false, 
        online: true 
    })
    gulp.watch('./src/js/*.js', gulp.series(['buildJs'])).on('change', () => browserSync.reload())
    gulp.watch('./src/scss/modules/*.scss', gulp.series(['buildCss'])).on('change', () => browserSync.reload())
    gulp.watch('./src/pages/*.pug', gulp.series(['buildHTML'])).on('change', () => browserSync.reload())
}); 

gulp.task('build', gulp.series('clean', gulp.parallel('buildImages', 'buildJs', 'buildCss','buildHTML')));
